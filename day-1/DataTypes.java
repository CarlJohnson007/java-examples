//class
public class DataTypes {

    //method
    /**
     * This is a main method
     * @param args - Array of String
     * @return void
     */
    public static void main(String[] args) {
        // comments
        // single line comment

        /*
         * This is a block level comment for multiple lines
        */

        /**
         * This is a documentation comments
         */

         // Data types
         /*
          * Numbers
                whole
                    byte (1 byte/ 8 bits)   
                    short (2 bytes/ 16 bits)
                    int (4 bytes/ 32 bits)
                    long (8 bytes/ 64 bits)

                real
                    float (4 bytes/ 32 bits)
                    double ( 8 bytes/ 64 bits)
                  
          * boolean
               true, false
          * char 
          */
        

          //data-type variable name = value;
           int numberOfStudents = 34;
           boolean attended = true ;
           float distanceFromOffice = 4.5F;
           char initial = 'C';

           //assign your attributes to variables 
           //age (int), gender- char, name- String, heightInFeet float
    }
}
