
public class VarialbeNamingConvention {
    public static void main(String[] args) {
        //Variable naming rules
        /*
         * Variable name cannot be a keyword
         *   - keywords in java - class, void, if, else, .... 52 keywords
         * Variable names cannot contain special characters apart from $ and _
         * Variable name cannot start with number
         */
        int value = 34;
        int valu_ = 34;

        //Not valid
        //int 34Value= 56;
        //int valu@ = 34;

        //convention
        /*
         * variable name should be used with camelcase
         * Letter should be capital in case of Class/Interface
         * Letter should be small in case of method, variable name 
         */
        //valid
        int numberOfStudents - 67;

        //Not recommended
        int NumberOfStudents_ = 85;
    }
}
