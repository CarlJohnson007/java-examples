package com.classpath.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		Set<String> namesOfEmployees = new HashSet<>();
		
		namesOfEmployees.add("Rama");
		namesOfEmployees.add("Rama");
		namesOfEmployees.add("Hari");
		namesOfEmployees.add("Hari");
		namesOfEmployees.add("Vinay");
		namesOfEmployees.add("Vinay");
		namesOfEmployees.add("Vinod");
		namesOfEmployees.add("Vinod");
		namesOfEmployees.add("mary");
		namesOfEmployees.add("mary");
		namesOfEmployees.add("Mithun");
		namesOfEmployees.add("Mithun");
		namesOfEmployees.add("Sakshi");
		
		System.out.println("Size of the set is "+ namesOfEmployees.size());
		
		Iterator<String> it = namesOfEmployees.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}

}
