package com.classpath.interfacedemo;

public interface PhoneRecharge {
	
	public abstract void recharge(String phoneNumber, double amount);

}
