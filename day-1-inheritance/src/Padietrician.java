
public class Padietrician extends Doctor{
	
	public Padietrician(String name, int age) {
		super(name, age);
	}
	
	public void treatKids() {
		System.out.println("Treating kids ..");
	}

	@Override
	public void treatPatient(String patientName) {
		treatKids();
		
	}

}
