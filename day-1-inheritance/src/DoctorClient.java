
public class DoctorClient {
	
	public static void main(String[] args) {
		//Data-type variable-name = new Data-Type();
		Doctor doctor = new OrthoPedician("Ramesh", 32);
		
		//syntax for calling the method..
		//doctor.treatPatient("John");
		
		//Valid rules 
		// Data-type values = new (any data type that passes IS-A test on the Data-type);
		Doctor doctor1 = new OrthoPedician("Ramesh", 34);
		Doctor doctor2 = new OrthoPedician("Ramesh", 34);
		Doctor doctor3 = new Padietrician("Ramesh", 34);
		

		//doctor1.treatPatient(); 
		//doctor2.treatPatient(); 
		//doctor3.treatPatient();
				 
		//this is not possible
		 //OrthoPedician ortho = new Doctor("Ramesh", 44);
		//OrthoPedician ortho = new Padietrician("Ramesh", 44);
		
		Doctor ortho = new OrthoPedician("Ramesh",38);
		Doctor dentist = new Dentist("Vinay",38);
		
		
		Doctor doc = new OrthoPedician("Harish", 45);
		
		//constructor overloading
		Doctor doc2 = new Dentist("Harish",24);
		
		dentist.treatPatient("Vinay");
		
	}

}
