
public class KneeSurgeon extends Doctor {

	public KneeSurgeon(String name, int age) {
		super(name, age);
	}

	@Override
	public void treatPatient(String patientName) {
		System.out.println("Conducting Knee surgery");
		
	}

}
