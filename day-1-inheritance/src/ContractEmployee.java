
public class ContractEmployee extends Employee{

	private int contractDurationInDays = 45;
	private double payPerHour = 3000;
	
	public ContractEmployee(String name) {
		super(name);
	}

	@Override
	public void applyForLeave(int noOfDays) {
		this.contractDurationInDays = this.contractDurationInDays + noOfDays;
	}

	@Override
	public double paySalary() {
		return this.payPerHour * 22;
	}
	
	public int getContractDuration() {
		return this.contractDurationInDays;
	}

}
